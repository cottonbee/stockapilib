RSpec.describe StockApiLib::LicenseService do
  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])

    @license_service = StockApiLib::LicenseService.new(
      configuration: config
    )

    authorization_service = StockApiLib::AuthorizationService.new(
      configuration: config
    )

    @authorization_data = authorization_service.get_authorization_data(
      expiration_timestamp: 1_747_079_786
    )
  end

=begin
  it 'should get member profile' do
    request = StockApiLib::LicenseRequest.new
    request.locale = 'en_US'
    request.content_id = 242287951
    request.license = 'Standard'

    res = @license_service.get_member_profile(request: request, access_token: @authorization_data.access_token)

    expect(res).to be_a_kind_of StockApiLib::LicenseResponse

  end


  it 'should get download asset url' do
    request = StockApiLib::LicenseRequest.new
    request.locale = 'en_US'
    request.content_id = 242287951
    request.license = 'Standard'

    res = @license_service.download_asset_url(
      request: request, access_token: @authorization_data.access_token
    )

    expect(res).to be_a_kind_of String

  end

  it 'should download asset ' do
    request = StockApiLib::LicenseRequest.new
    request.locale = 'en_US'
    request.content_id = 242287951
    request.license = 'Standard'

    res = @license_service.download_asset(
      request: request, access_token: @authorization_data.access_token
    )

    expect(res).to be_a_kind_of String

  end

  it 'should get license info' do
    request = StockApiLib::LicenseRequest.new
    request.locale = 'en_US'
    request.content_id = 242287951
    request.license = 'Standard'

    res = @license_service.get_content_info(request: request, access_token: @authorization_data.access_token)

    expect(res).to be_a_kind_of StockApiLib::LicenseResponse

  end
=end

  it 'should get license ' do
    request = StockApiLib::LicenseRequest.new
    request.locale = 'en_US'
    request.content_id = 242287951
    request.license = 'Standard'
    request.license_again = false

    res = @license_service.get_content_license(
      request: request, access_token: @authorization_data.access_token
    )

    expect(res).to be_a_kind_of StockApiLib::LicenseResponse

  end

end
