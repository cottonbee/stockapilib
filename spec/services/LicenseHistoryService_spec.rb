RSpec.describe StockApiLib::LicenseHistoryService do
  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])

    @license_history_service = StockApiLib::LicenseHistoryService.new(
      configuration: config
    )

    authorization_service = StockApiLib::AuthorizationService.new(
      configuration: config
    )

    @authorization_data = authorization_service.get_authorization_data(
      expiration_timestamp: 1_747_079_786
    )
  end

  it 'simplest search request should succeed' do
    request = StockApiLib::LicenseHistoryRequest.new
    request.locale = 'en_US'
    request.search_parameters = StockApiLib::SearchParamLicenseHistory.new
    request.search_parameters.limit = 20

    res = @license_history_service.get_license_history_files(
      request: request,
      access_token: @authorization_data.access_token
    )

    expect(res.status).to eq 200
    expect(res.body).not_to be_nil
  end
end