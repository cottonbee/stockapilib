require 'yaml'
require 'mongo'

RSpec.describe StockApiLib::StockApiService do
  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])

    mongo_config = Hash.new
    mongo_config['adapter'] = 'MongoDB'
    mongo_config['encoding'] = 'unicode'
    mongo_config['host'] = 'localhost'
    mongo_config['port'] = 27_017
    mongo_config['database'] = 'adobeStockCacheTests'


    mongo_db = Mongo::Client.new(
      ["#{mongo_config['host']}:#{mongo_config['port']}"],
      database: mongo_config['database']
    )
    @service = StockApiLib::StockApiService.new(
      configuration: config,
      mongo_db: mongo_db
    )
  end

  #it 'should get one image' do
  #  res = @service.get_file(id: 71_182_279, locale: 'en_US')
  #  expect(res).to be_a_kind_of StockApiLib::StockFile
  #end

  it 'simplest search request should succeed' do
    request = StockApiLib::SearchFilesRequest.new
    request.locale = 'en_US'
    request.search_parameters = StockApiLib::SearchParameters.new
    request.search_parameters.filters = StockApiLib::SearchParametersFilters.new
    request.search_parameters.filters.content_type_all = false
    request.search_parameters.filters.content_type_illustration = true
    request.search_parameters.filters.content_type_vector = true
    request.search_parameters.limit = 10

    request.result_columns = StockApiLib::StockFile.attributes
    request.result_columns.delete(:is_licensed)
    request.result_columns.delete(:asset_type_id)
    request.result_columns << :nb_results
    request.similar_image = nil

    res = @service.get_files(request: request, access_token: nil)

    expect(res).to be_a_kind_of StockApiLib::SearchFilesResponse
  end
end
