RSpec.describe StockApiLib::SearchCategoryService do
  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])

    @search_files_service = StockApiLib::SearchFilesService.new(
      configuration: config
    )
  end

  it 'simplest search request should succeed' do
    request = StockApiLib::SearchFilesRequest.new
    request.locale = 'en_US'
    request.search_parameters = StockApiLib::SearchParameters.new
    request.search_parameters.filters = StockApiLib::SearchParametersFilters.new
    request.search_parameters.filters.content_type_all = true
    request.search_parameters.media_id = 71_182_279
    request.search_parameters.limit = 10

    request.result_columns = StockApiLib::StockFile.attributes
    request.result_columns.delete(:is_licensed)
    request.result_columns.delete(:asset_type_id)
    request.result_columns << :nb_results
    request.similar_image = nil

    res = @search_files_service.get_files(request: request, access_token: nil)

    expect(res.status).to eq 200
    expect(res.body).not_to be_nil
  end
end
