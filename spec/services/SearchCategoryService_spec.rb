RSpec.describe StockApiLib::SearchCategoryService do

  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])

    @search_category_service = StockApiLib::SearchCategoryService.new(
      configuration: config
    )
  end

  it 'gets category' do
    search_category_request = StockApiLib::SearchCategoryRequest.new
    search_category_request.category_id = 47
    search_category_request.locale = 'en_US'

    res = @search_category_service.get_category(
      request: search_category_request, access_token: nil
    )

    expect(res.status).to eq 200
    expect(res.body).not_to be_nil
  end

  it 'gets categories tree' do
    search_category_request = StockApiLib::SearchCategoryRequest.new
    search_category_request.category_id = nil
    search_category_request.locale = 'en_US'

    res = @search_category_service.get_category_tree(
      request: search_category_request, access_token: nil
    )

    expect(res.status).to eq 200
    expect(res.body).not_to be_nil
  end
end
