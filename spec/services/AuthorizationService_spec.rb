require 'yaml'

RSpec.describe StockApiLib::AuthorizationService do
  before :all do
    yaml = YAML.safe_load(File.read(Dir.pwd + '/lib/stock_api_lib.yml'))
    config = StockApiLib::Configuration.new(data: yaml['stock_api_lib'])
    @authorization_service = StockApiLib::AuthorizationService.new(
      configuration: config
    )
  end

  it 'should authorize only once' do
    authorization_data1 = @authorization_service.get_authorization_data(
      expiration_timestamp: 1_747_079_786
    )

    authorization_data2 = @authorization_service.get_authorization_data(
      expiration_timestamp: 1_747_079_786
    )

    expect(authorization_data1).to_not be_nil
    expect(authorization_data2).to_not be_nil

    expect(authorization_data1).to eq(authorization_data2)
  end
end