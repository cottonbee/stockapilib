module StockApiLib
  class LicenseHistoryRequest < ParsableModel
    attr_accessor :locale, # string
                  :search_parameters, # SearchParamLicenseHistory
                  :result_columns # array
  end
end
