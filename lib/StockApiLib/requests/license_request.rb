module StockApiLib
  class LicenseRequest < ParsableModel
    attr_accessor :locale, # string
                  :content_id, # int
                  :license, # string
                  :state, # string
                  :format, # string
                  :license_reference, # array (LicenseReference)
                  :license_again
    attr_mappings license_reference: [LicenseReference]
  end
end
