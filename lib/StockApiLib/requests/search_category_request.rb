module StockApiLib
  class SearchCategoryRequest < ParsableModel
    attr_accessor :locale, # string
                  :category_id # int
  end
end
