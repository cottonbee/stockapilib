module StockApiLib
  class SearchFilesRequest < ParsableModel
    attr_accessor :locale, # string
                  :search_parameters, # SearchParameters
                  :result_columns, # array
                  :similar_image # string
  end
end