require 'StockApiLib/models/stock_file_license_history'

module StockApiLib
  class LicenseHistoryResponse < ParsableModel
    attr_accessor :nb_results, # int
                  :files  # array / StockFileLicenseHistory

    attr_mappings files: [StockFileLicenseHistory]
  end
end
