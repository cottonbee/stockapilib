require 'StockApiLib/models/stock_file'

module StockApiLib
  class SearchFilesResponse < ParsableModel
    attr_accessor :nb_results, # int
                  :files # array / StockFile

    attr_mappings files: [StockFile]
  end
end
