module StockApiLib
  class SearchCategoryResponse < ParsableModel
    attr_accessor :id, # int
                  :name, # string
                  :link # string

  end
end
