require 'StockApiLib/models/license_entitlement'
require 'StockApiLib/models/license_purchase_options'
require 'StockApiLib/models/license_member_info'
require 'StockApiLib/models/license_reference'
require 'StockApiLib/models/license_content'

module StockApiLib
  class LicenseResponse < ParsableModel
    attr_accessor :available_entitlement, # LicenseEntitlement
                  :purchase_options, # LicensePurchaseOptions
                  :member, # LicenseMemberInfo
                  :cce_agency, # array
                  :contents # array

    attr_mappings available_entitlement: LicenseEntitlement,
                  purchase_options: LicensePurchaseOptions,
                  member: LicenseMemberInfo,
                  cce_agency: [LicenseReference],
                  contents: { String: LicenseContent }
  end
end
