module StockApiLib
  class StockFileCompProp < ParsableModel
    attr_accessor :width, # int
                  :height, # int
                  :url # string
  end
end
