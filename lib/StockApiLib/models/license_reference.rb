module StockApiLib
  class LicenseReference < ParsableModel
    attr_accessor :id, # int
                  :value # string
  end
end
