module StockApiLib
  class LicenseEntitlementQuota < ParsableModel
    attr_accessor :image_quota, # int
                  :video_quota, # int
                  :credits_quota, # int
                  :standard_credits_quota, # int
                  :premium_credits_quota, # int
                  :universal_credits_quota # int
  end
end
