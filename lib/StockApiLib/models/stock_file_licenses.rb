require 'StockApiLib/models/stock_file_license_prop'

module StockApiLib
  class StockFileLicenses < ParsableModel
    attr_accessor :Standard,
                  :Standard_M

    attr_mappings Standard: StockFileLicenseProp,
                  Standard_M: StockFileLicenseProp
  end
end
