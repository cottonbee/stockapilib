require 'StockApiLib/models/license_entitlement_quota'

module StockApiLib
  class LicenseEntitlement < ParsableModel
    attr_accessor :quota, # int
                  :license_type_id, # int
                  :has_credit_model, # bool
                  :has_agency_model, # bool
                  :is_cce, # bool
                  :full_entitlement_quota # LicenseEntitlementQuota

    attr_mappings full_entitlement_quota: LicenseEntitlementQuota
  end
end
