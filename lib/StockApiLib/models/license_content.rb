require 'StockApiLib/models/license_purchase_details'
require 'StockApiLib/models/license_comp'
require 'StockApiLib/models/license_thumbnail'

module StockApiLib
  class LicenseContent < ParsableModel
    attr_accessor :content_id, # string
                  :purchase_details, # LicensePurchaseDetails
                  :size, # string
                  :comp, # LicenseComp
                  :thumbnail # LicenseThumbnail

    attr_mappings purchase_details: LicensePurchaseDetails,
                  comp: LicenseComp,
                  thumbnail: LicenseThumbnail
  end
end
