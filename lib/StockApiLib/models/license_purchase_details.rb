module StockApiLib
  class LicensePurchaseDetails < ParsableModel
    attr_accessor :date, # string
                  :cancelled, # string
                  :url, # string
                  :content_type, # string
                  :width, # int
                  :height, # int
                  :frame_rate, # float
                  :content_length, # int
                  :duration, # int
                  :license, # string
                  :state # string
  end
end
