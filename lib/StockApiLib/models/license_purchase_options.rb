module StockApiLib
  class LicensePurchaseOptions < ParsableModel
    attr_accessor :state, # string
                  :requires_checkout, # bool
                  :message, # string
                  :url # string
  end
end
