module StockApiLib
  class ParsableModel
    def self.attr_accessor(*vars)
      @attributes ||= []
      @attributes.concat vars
      super(*vars)
    end

    def self.attr_mappings(vars)
      @mappings ||= {}
      @mappings = @mappings.merge vars
    end

    def self.attr_hash_exceptions(vars)
      @exceptions ||= {}
      @exceptions = @exceptions.merge vars
    end

    def self.hash_exceptions
      @exceptions
    end

    class << self
      attr_reader :attributes
    end

    def attributes
      self.class.attributes
    end

    def self.from(json:)
      json = JSON.parse json if json.is_a? String
      return nil if json.nil?

      object = new
      object.attributes.each do |variable_name|
        value = json[variable_name.to_s] if json.is_a? Hash
        if !@mappings.nil? && !@mappings[variable_name].nil?
          if !@mappings[variable_name].is_a?(Hash) &&
             !@mappings[variable_name].is_a?(Array) &&
             @mappings[variable_name] < ParsableModel
            value = @mappings[variable_name].from(json: value)
          end

          # parsowanie hasha w odpowiedzi
          if @mappings[variable_name].is_a?(Hash) && value.is_a?(Hash) &&
             @mappings[variable_name].length == 1
            hash_objects_class = @mappings[variable_name].values.first
            if hash_objects_class < ParsableModel
              value = value.transform_values do |value_element|
                hash_objects_class.from(json: value_element)
              end
            end

          end

          # parsowanie tablicy w odpowiedzi
          if @mappings[variable_name].is_a?(Array) && value.is_a?(Array) &&
             @mappings[variable_name].length == 1
            array_objects_class = @mappings[variable_name][0]
            if array_objects_class < ParsableModel
              value = value.map do |value_element|
                array_objects_class.from(json: value_element)
              end
            end
          end

        end
        object.instance_variable_set('@' + variable_name.to_s, value)
      end
      object
    end

    def to_hash(top_level: true)
      hash = {}
      attributes.each do |variable_name|
        value = instance_variable_get('@' + variable_name.to_s)
        variable_name = if self.class.hash_exceptions.nil? ||
                           self.class.hash_exceptions[variable_name].nil?
                          variable_name.to_s
                        else
                          self.class.hash_exceptions[variable_name]
                        end

        value = value.to_hash(top_level: false) if value.is_a? ParsableModel
        unless value.nil?
          if value.is_a? Array
            hash[variable_name + '[]'] = value if top_level
            hash[variable_name + ']['] = value unless top_level
          elsif value.is_a? Hash
            hash.merge! prepare_hash_param(
              hash: value,
              variable_name: variable_name,
              top_level: top_level
            )
          else
            hash[variable_name] = value
          end
        end
      end
      return nil if hash.empty?

      hash
    end

    def prepare_hash_param(hash:, variable_name:, top_level:)
      new_hash = {}
      hash.each do |key, value|
        new_hash[variable_name + '[' + key + ']'] = value if top_level
        new_hash['' + variable_name + '][' + key + ''] = value unless top_level
      end
      new_hash
    end
  end
end
