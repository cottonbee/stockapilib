module StockApiLib
  class SearchParamLicenseHistory < ParsableModel
    attr_accessor :limit, # int
                  :offset, # offset
                  :thumbnail_size # int
  end
end
