module StockApiLib
  class StockFileLicenseProp < ParsableModel
    attr_accessor :width, # int
                  :height, # int
                  :url # string
  end
end
