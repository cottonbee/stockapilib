module StockApiLib
  class AuthorizationData
    attr_reader :token_type, :access_token

    def initialize(token_type:, expires_in:, access_token:)
      @token_type = token_type
      @access_token = access_token
      @expires_in = expires_in
      @expiration_timestamp = Time.now.getutc.to_i + expires_in
    end

    def expired?
      @expiration_timestamp < Time.now.getutc.to_i
    end

    def self.from(json:)
      json = JSON.parse(json, symbolize_names: true) if json.is_a? String
      AuthorizationData.new(token_type: json[:token_type],
                                   expires_in: json[:expires_in],
                                   access_token: json[:access_token])

    end
  end
end
