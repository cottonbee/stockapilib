module StockApiLib
  class StockFileLicenseHistory < ParsableModel
    attr_accessor :license, # string
                  :license_date, # string
                  :download_url, # string
                  :id, # int
                  :title, # string
                  :creator_id, # int
                  :creator_name, # string
                  :vector_type, # string
                  :content_type, # string
                  :media_type_id, # int
                  :width, # int
                  :height, # int
                  :content_url, # string
                  :details_url, # string
                  :thumbnail_url, # string
                  :thumbnail_html_tag, # string
                  :thumbnail_width, # int
                  :thumbnail_height, # int
                  :thumbnail_110_url, # string
                  :thumbnail_110_width, # int
                  :thumbnail_110_height, # int
                  :thumbnail_160_url, # string
                  :thumbnail_160_width, # int
                  :thumbnail_160_height, # int
                  :thumbnail_220_url, # string
                  :thumbnail_220_width, # int
                  :thumbnail_220_height, # int
                  :thumbnail_240_url, # string
                  :thumbnail_240_width, # int
                  :thumbnail_240_height, # int
                  :thumbnail_500_url, # string
                  :thumbnail_500_width, # int
                  :thumbnail_500_height, # int
                  :thumbnail_1000_url, # string
                  :thumbnail_1000_width, # int
                  :thumbnail_1000_height # int
  end

end
