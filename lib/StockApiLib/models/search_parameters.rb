module StockApiLib
  class SearchParametersFilters < ParsableModel
    attr_accessor :colors, # string / [filters][colors]
                  :area_pixels, # int / [filters][area_pixels]
                  :content_type_photos, # int / [filters][content_type:photo]
                  :content_type_illustration, # int / [filters][content_type:illustration]
                  :content_type_vector, # int / [filters][content_type:vector]
                  :content_type_video, # int / [filters][content_type:video]
                  :content_type_template, # int / [filters][content_type:template]
                  :content_type_3d, # int / [filters][content_type:3D]
                  :content_type_all, # int / [filters][content_type:all]
                  :editorial, # int / [filters][editorial]]
                  :offensive_2, # int / [filters][offensive:2]
                  :isolated_on, # int / [filters][isolated:on]
                  :panoramic_on, # int / [filters][panoramic:on]
                  :thumbnail_size, # int / [filters][thumbnail_size]
                  :orientation, # string / [filters][orientation]
                  :age, # int / [filters][age]
                  :video_duration, # array / [filters][video_duration]
                  :template_type_id, # array / [filters][template_type_id][]
                  :type_3d_id, # array / [filters][3d_type_id][]
                  :template_category_id, # array / [filters][template_category_id][]
                  :premium, # string [filters][premium]
                  :has_releases # string / [filters][has_releases]

    attr_hash_exceptions content_type_photos: 'content_type:photo',
                         content_type_illustration: 'content_type:illustration',
                         content_type_vector: 'content_type:vector',
                         content_type_video: 'content_type:video',
                         content_type_template: 'content_type:template',
                         content_type_3d: 'content_type:3D',
                         content_type_all: 'content_type:all',
                         offensive_2: 'offensive:2',
                         isolated_on: 'isolated:on',
                         panoramic_on: 'panoramic:on',
                         type_3d_id: 'd_type_id'
  end

  class SearchParameters < ParsableModel
    attr_accessor :creator_id, # int
                  :media_id, # int
                  :model_id, # int
                  :serie_id, # int
                  :similar, # int
                  :category, # int
                  :limit, # int
                  :offset, # int
                  :words, # string
                  :similar_url, # string
                  :gallery_id, # string
                  :similar_image, # int
                  :order, # string
                  :filters

    attr_mappings filters: SearchParametersFilters
  end
end
