module StockApiLib
  class LicenseReferenceResponse
    attr_accessor :id, # int
                  :text, # string
                  :required # bool
  end
end
