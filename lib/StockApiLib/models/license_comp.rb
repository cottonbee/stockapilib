module StockApiLib
  class LicenseComp
    attr_accessor :url, # string
                  :content_type, # string
                  :width, # int
                  :height, # int
                  :frame_rate, # float
                  :content_length, # int
                  :duration # int

  end
end
