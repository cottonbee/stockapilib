module StockApiLib
  class ApiError < ParsableModel
    attr_accessor :error_code,
                  :message,
                  :case,
                  :status
  end
end
