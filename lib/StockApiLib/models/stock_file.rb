require 'StockApiLib/models/stock_file_comps'
require 'StockApiLib/models/stock_file_licenses'
require 'StockApiLib/models/keyword'

module StockApiLib
  class StockFile < ParsableModel
    attr_accessor :id, # int
                  :title, # string
                  :creator_id, # int
                  :creator_name, # string
                  :creation_date, # string
                  :country_name, # string
                  :thumbnail_url, # string
                  :thumbnail_html_tag, # string
                  :thumbnail_width, # int
                  :thumbnail_height, # int
                  :thumbnail_110_url, # string
                  :thumbnail_110_width, # int
                  :thumbnail_110_height, # int
                  :thumbnail_160_url, # string
                  :thumbnail_160_width, # int
                  :thumbnail_160_height, # int
                  :thumbnail_220_url, # string
                  :thumbnail_220_width, # int
                  :thumbnail_220_height, # int
                  :thumbnail_240_url, # string
                  :thumbnail_240_width, # int
                  :thumbnail_240_height, # int
                  :thumbnail_500_url, # string
                  :thumbnail_500_width, # int
                  :thumbnail_500_height, # int
                  :thumbnail_1000_url, # string
                  :thumbnail_1000_width, # int
                  :thumbnail_1000_height, # int
                  :media_type_id, # int
                  :width, # int
                  :height, # int
                  :is_licensed, # bool
                  :comp_url, # string
                  :comp_width, # int
                  :comp_height, # int
                  :nb_views, # int
                  :nb_downloads, # int
                  :category, # SearchCategoryResponse
                  :keywords, # array
                  :has_releases, # bool
                  :asset_type_id, # int
                  :vector_type, # string
                  :content_type, # string
                  :framerate, # float
                  :duration, # int
                  :stock_id, # int
                  :comps, # StockFileComps
                  :details_url, # string
                  :template_type_id, # int
                  #:template_category_ids, # array
                  :marketing_text, # string
                  :description, # string
                  :size_bytes, # int
                  :premium_level_id, # int
                  :is_premium, # bool
                  :licenses, # StockFileLicenses
                  :video_preview_url, # string
                  :video_preview_height, # int
                  :video_preview_width, # int
                  :video_preview_content_length, # int
                  :video_preview_content_type, # string
                  :video_small_preview_url, # string
                  :video_small_preview_height, # int
                  :video_small_preview_width, # int
                  :video_small_preview_content_length, # int
                  :video_small_preview_content_type # string

    attr_mappings category: SearchCategoryResponse,
                  comps: StockFileComps,
                  licenses: StockFileLicenses,
                  keywords: [Keyword]
  end
end
