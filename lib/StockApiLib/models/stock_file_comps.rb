require 'StockApiLib/models/stock_file_comp_prop'

module StockApiLib
  class StockFileComps < ParsableModel
    attr_accessor :Standard, # StockFileCompPropModels
                  :Video_HD, # StockFileCompPropModels
                  :Video_4K # StockFileCompPropModels

    attr_mappings Standard: StockFileCompProp,
                  Video_HD: StockFileCompProp,
                  Video_4K: StockFileCompProp

  end
end
