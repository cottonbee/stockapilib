module StockApiLib
  class LicenseThumbnail
    attr_accessor :url, # string
                  :content_type, # string
                  :width, # int
                  :height # int
  end
end
