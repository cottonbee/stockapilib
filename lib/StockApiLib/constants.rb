module StockApiLib
  class Constants
    ENDPOINTS = {
      jwt: 'https://ims-na1.adobelogin.com/ims/exchange/v1/jwt',
      search: 'https://stock.adobe.io/Rest/Media/1/Search/Files',
      category: 'https://stock.adobe.io/Rest/Media/1/Search/Category',
      category_tree: 'https://stock.adobe.io/Rest/Media/1/Search/CategoryTree',
      license: 'https://stock.adobe.io/Rest/Libraries/1/Content/License',
      license_info: 'https://stock.adobe.io/Rest/Libraries/1/Content/Info',
      user_profile: 'https://stock.adobe.io/Rest/Libraries/1/Member/Profile',
      abandon: 'https://stock.adobe.io/Rest/Libraries/1/Member/Abandon',
      license_history: 'https://stock.adobe.io/Rest/Libraries/1/Member/LicenseHistory'
    }.freeze

    ENVIRONMENTS = %i[prod stage].freeze

    SEARCH_PARAMETERS = {
      thumbnail_size: [
        110,
        160,
        240,
        500,
        1000
      ],
      order: %w[
        relevance
        creation
        popularity
        nb_downloads
        undiscovered
      ],
      filters_premium: %w[
        false
        true
        all
      ],
      filters_3d_type_id: [
        1,
        2,
        3
      ],
      filters_template_type_id: [
        1,
        2,
        3,
        4,
        5
      ],
      filters_has_releases: %w[
        true
        false
        all
      ],
      filters_orientation: %w[
        horizontal
        vertical
        square
        all
      ],
      filters_age: %w[
        1w
        1m
        6m
        1y
        2y
        all
      ],
      filters_video_duration: %w[10 20 30 30- all]

    }.freeze

    RESULT_COLUMNS = {
      stock_file_license_history: %i[thumbnail_110_url
                                     thumbnail_110_height
                                     thumbnail_110_width
                                     thumbnail_160_url
                                     thumbnail_160_height
                                     thumbnail_160_width
                                     thumbnail_220_url
                                     thumbnail_220_height
                                     thumbnail_220_width
                                     thumbnail_240_url
                                     thumbnail_240_height
                                     thumbnail_240_width
                                     thumbnail_500_url
                                     thumbnail_500_height
                                     thumbnail_500_width
                                     thumbnail_1000_url
                                     thumbnail_1000_height
                                     thumbnail_1000_width].freeze,
      stock_file: %i[
        nb_results
        id
        title
        creator_name
        creator_id
        country_name
        width *height
        thumbnail_url
        thumbnail_html_tag
        thumbnail_width
        thumbnail_height
        thumbnail_110_url
        thumbnail_110_width
        thumbnail_110_height
        thumbnail_160_url
        thumbnail_160_width
        thumbnail_160_height
        thumbnail_220_url
        thumbnail_220_width
        thumbnail_220_height
        thumbnail_240_url
        thumbnail_240_width
        thumbnail_240_height
        thumbnail_500_url
        thumbnail_500_width
        thumbnail_500_height
        thumbnail_1000_url
        thumbnail_1000_width
        thumbnail_1000_height
        media_type_id
        category
        category_hierarchy
        nb_views
        nb_downloads
        creation_date
        keywords
        has_releases
        comp_url
        comp_width
        comp_height
        is_licensed
        vector_type
        content_type
        framerate
        duration
        comps
        details_url
        template_type_id
        template_category_ids
        marketing_text
        description
        size_bytes
        premium_level_id
        is_premium
        licenses
        video_preview_url
        video_preview_width
        video_preview_height
        video_preview_content_length
        video_preview_content_type
        video_small_preview_url
        video_small_preview_width
        video_small_preview_height
        video_small_preview_content_length
        video_small_preview_content_type
      ]
    }.freeze

    LICENSE_TYPE = [
      'Standard', 'Extended', 'Video_HD', 'Video_4K', 'Standard_M', ''
    ].freeze

    MEDIA_TYPE_ID = {
      1 => 'Photos',
      2 => 'Illustrations',
      3 => 'Vectors',
      4 => 'Videos',
      6 => '3D',
      7 => 'Templates'
    }.freeze

    VECTOR_TYPE = %w[svg zip].freeze

    TYPE_3D_ID = {
      1 => 'Models',
      2 => 'Lights',
      3 => 'Materials'
    }.freeze

    TEMPLATE_TYPE_ID = {
      1 => 'PSDT',
      2 => 'AIT',
      3 => 'INDT',
      4 =>'PPRO Motion Graphics Template',
      5 =>'AE Motion Graphics Template'
    }.freeze

    PREMIUM_LEVEL_ID = {
      0 => 'Core/standard',
      1 => 'Free',
      2 => 'Premium level 1',
      3 => 'Premium level 2',
      4 => 'Premium level 3'
    }.freeze

    ERROR_CODE = {
      10 => 'Invalid access token. The access token that you passed is invalid or expired.',
      11 => 'Invalid API Key. The API key that you passed is not valid or has expired.',
      20 => 'Invalid parameters. The URL parameters that you passed are not supported.',
      31 => 'Invalid Method. The method that you specified does not exist in the method list.',
      100 => 'Invalid data. Data that you specified as arguments are not supported.'
    }.freeze
  end
end
