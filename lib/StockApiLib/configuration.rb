module StockApiLib
  class Configuration
    attr_reader :iss,
                :sub,
                :aud,
                :private_cert_path,
                :auth_url,
                :client_id,
                :client_secret,
                :api_key, # string
                :product, # string
                :target_env, # string
                :endpoints # array

    def initialize(data:, environment: :prod, constants: Constants)
      @iss = data['iss']
      @sub = data['sub']
      @aud = data['aud']
      unless File.exist?(Dir.pwd + data['private_cert_path'])
        raise 'No private key path'
      end

      @private_cert_path = Dir.pwd + data['private_cert_path']
      @auth_url = data['auth_url']
      @client_id = data['client_id']
      @client_secret = data['client_secret']
      @api_key = data['api_key']
      @product = data['product']
      environment = :prod if environment.nil?
      @target_env = environment.equal?(:prod) ? :prod : :stage
      @endpoints = prepare_endpoints(
        environment: target_env, constants: constants
      )
    end
    #     def initialize(
    #       iss:,
    #       sub:,
    #       aud:,
    #       private_cert_path:,
    #       auth_url:,
    #       client_id:,
    #       client_secret:,
    #       api_key:, product:,
    #       environment: :prod, constants: Constants)
    #
    #       @iss = iss
    #       @sub = sub
    #       @aud = aud
    #       @private_cert_path = private_cert_path
    #       @auth_url = auth_url
    #       @client_id = client_id
    #       @client_secret = client_secret
    #
    #       environment = :prod if environment.nil?
    #       @target_env = environment.equal?(:prod) ? :prod : :stage
    #       @endpoints = prepare_endpoints(
    #         environment: target_env, constants: constants
    #       )
    #       @api_key = api_key
    #       @product = product
    #     end

    def initialized?
      target_env.present? && endpoints.present? &&
        api_key.present? && product.present?
    end

    private

    def prepare_endpoints(environment:, constants:)
      endpoints = constants::ENDPOINTS
      return endpoints if environment.equal?(:prod)

      endpoints.merge(endpoints) do |_k, v|
        v.gsub! 'stock', 'stock-stage'
      end
    end
  end
end
