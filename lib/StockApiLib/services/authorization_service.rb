module StockApiLib
  class AuthorizationService
    # @param [Configuration] configuration
    # @param [HttpService] http_service
    def initialize(
      configuration:, http_service: HttpService.new
    )
      @http_service = http_service
      @configuration = configuration
    end

    # @return [AuthorizationData]
    def get_authorization_data(expiration_timestamp:, force_refresh: false)
      if force_refresh ||
         @authorization_data.nil? || !(@authorization_data.is_a? AuthorizationData) || @authorization_data.expired?
        @authorization_data = authorize(
          expiration_timestamp: expiration_timestamp
        )
      end
      @authorization_data
    end

    private

    # @return [AuthorizationData]
    def authorize(expiration_timestamp:)
      jwt_payload = {
        exp: expiration_timestamp,
        iss: @configuration.iss,
        sub: @configuration.sub,
        "https://ims-na1.adobelogin.com/s/ent_stocksearch_sdk": true,
        aud: @configuration.aud
      }

      jwt_token = JWTService.new(
        private_key_path: @configuration.private_cert_path,
        payload: jwt_payload
      ).generate_encoded_jwt

      params = {
        jwt_token: jwt_token,
        client_id: @configuration.client_id,
        client_secret: @configuration.client_secret
      }

      response = @http_service.post(
        url: @configuration.auth_url,
        headers: {},
        body: params
      )
      prepare(response: response)
    end

    def prepare(response:)
      json = response.body
      return AuthorizationData.from(json: json) if response.status == 200

      error = ApiError.new
      error.error_code = response.status
      error.message = response.body
      error.case = 'JSON_PARSE_ERROR'
      return error

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response.status
      error.message = response.body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end
  end
end
