require 'StockApiLib/requests/search_files_request'

module StockApiLib
  class SearchFilesService
    include StockApiLib::CommonApiHeaders

    def initialize(configuration:, http_service: HttpService.new)
      @configuration = configuration
      @http_service = http_service
    end

    # @param [SearchFilesRequest] request
    # @param [String] access_token
    # @return [SearchFilesResponse]
    def get_files(request:, access_token:)
      validate_search_params(request: request, access_token: access_token)
      endpoint = @configuration.endpoints[:search]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(configuration: @configuration,
                                            access_token: access_token)
      call_get_files(request: request, headers: headers, url: url)
    end

    def prepare(response_body:, response_status: 200)
      json = response_body
      return SearchFilesResponse.from(json: json) if response_status == 200

      ApiError.from(json: json)

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response_status
      error.message = response_body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end

    private

    def call_get_files(request:, headers:, url:)
      image_blob = compare_image(similar_image_path: request.similar_image)
      if image_blob.nil?
        @http_service.get(url: url, headers: headers)
      else
        @http_service.post_multipart(
          url: url, headers: headers, filename_or_io: image_blob
        )
      end
    end

    # @param [String] access_token
    # @param [SearchFilesRequest] request
    def validate_search_params(request:, access_token: nil)
      if request.search_parameters.nil?
        raise 'Search parameter must be present in the request object'
      end

      validate_is_licensed_column(request: request, access_token: access_token)

      if request.search_parameters.similar_image == 1 &&
        request.similar_image.nil?
        raise 'Image Data missing! Search parameter similar_image '\
              'requires similar_image in query parameters'
      end
    end

    def validate_is_licensed_column(request:, access_token:)
      if !request.result_columns.nil? && !request.result_columns.empty? &&
        request.result_columns.include?(:is_licensed) &&
        access_token.nil?
        raise 'Access Token missing! Result Column ' \
              'is_licensed requires authentication'
      end
    end

    def compare_image(similar_image_path:)
      return nil if similar_image_path.nil? || !File.exist?(similar_image_path)

      extension = File.extname(similar_image_path).downcase
      right_ext = %w[png jpeg jpeg gif].include?(extension)
      raise 'Only jpg, png and gif are supported image formats' unless right_ext

      File.read(similar_image_path)
    end
  end
end
