require 'jwt'

module StockApiLib
  class JWTService
    def initialize(private_key_path:, payload:)
      @private_key_path = private_key_path
      @payload = payload
    end

    def generate_encoded_jwt
      private_key = File.read(@private_key_path)
      rsa_private = OpenSSL::PKey::RSA.new(private_key)
      JWT.encode @payload, rsa_private, 'RS256'
    end
  end
end
