module StockApiLib
  class LicenseService < ParsableModel
    include StockApiLib::CommonApiHeaders

    def initialize(configuration:, http_service: HttpService.new)
      @configuration = configuration
      @http_service = http_service
    end

    def get_content_info(request:, access_token:)
      validate_params(request: request, access_token: access_token)
      validate_license(request)

      endpoint = @configuration.endpoints[:license_info]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: access_token
      )
      raw_response = call_get_content_info(headers, request, url)

      prepare(response: raw_response)
    end

    def get_content_license(request:, access_token:)
      validate_params(request: request, access_token: access_token)
      validate_license(request)

      endpoint = @configuration.endpoints[:license]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: access_token
      )
      raw_response = @http_service.get(url: url, headers: headers)

      prepare(response: raw_response)
    end

    def get_member_profile(request:, access_token:)
      validate_params(request: request, access_token: access_token)
      validate_license(request)

      endpoint = @configuration.endpoints[:user_profile]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: access_token
      )

      raw_response = @http_service.get(url: url, headers: headers)

      prepare(response: raw_response)
    end

    # @param [LicenseRequest] request
    # @param [String] access_token
    # @return [Boolean]
    def abandon_license(request:, access_token:)
      validate_params(request: request, access_token: access_token)
      endpoint = @configuration.endpoints[:abandon]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: access_token
      )

      raw_response = @http_service.get(url: url, headers: headers)
      raw_response.status == 204
    end

    def download_asset_url(request:, access_token:)
      prepare_download_asset_request_url(
        request: request,
        access_token: access_token
      )
    end

    def download_asset(request:, access_token:)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: nil
      )
      url = prepare_download_asset_request_url(
        request: request,
        access_token: access_token
      )
      raw_response = @http_service.get(url: url, headers: headers)
      raw_response.body
    end

    private

    def validate_license(request)
      if request.license.nil?
        raise 'Licensing state must be present in the license request'
      end
    end

    def call_get_content_info(headers, request, url)
      if request.license_reference.nil?
        @http_service.get(url: url, headers: headers)
      else
        @http_service.post(
          url: url,
          headers: headers,
          body: request.license_reference.to_hash
        )
      end
    end

    def prepare_download_asset_request_url(request:, access_token:)
      content_info = get_content_info(request: request, access_token: access_token)
      content_id = request.content_id.to_s
      license_content = content_info.contents[content_id]
      purchase_details = license_content.purchase_details
      if purchase_details.nil? || purchase_details.state.nil?
        raise 'Could not find the purchase details for the asset'
      end

      if purchase_details.state != 'purchased'
        member_profile = get_member_profile(
          request: request, access_token: access_token
        )
        if member_profile.available_entitlement.nil?
          raise 'Could not find the available licenses for the user'
        end

        if member_profile.purchase_options.nil?
          raise 'Could not find the user purchasing options for the asset'
        end

        can_buy = member_profile.available_entitlement.quota != 0 ||
                  member_profile.purchase_options.state == 'overage'

        if can_buy
          raise 'Content not licensed but have enough quota or overage plan, so first buy the license'
        else
          raise 'Content not licensed and also you do not have enough quota or overage plan'
        end

      end

      content_license = get_content_license(request: request, access_token: access_token)

      content_id = request.content_id.to_s
      license_content = content_license.contents[content_id]
      purchase_details = license_content.purchase_details

      if purchase_details.nil? || purchase_details.url.nil?
        raise 'Could not find the purchase details for the asset'
      end

      url = purchase_details.url
      url + '?token=' + access_token
    end

    def validate_params(request:, access_token:)
      if request.nil? || request.content_id.nil?
        raise 'Asset Content id must be present in the license request'
      end
      if access_token.nil? || access_token.empty?
        return 'Access token can\'t be empty'
      end
    end

    def prepare(response:)
      json = response.body
      return LicenseResponse.from(json: json) if response.status == 200

      ApiError.from(json: json)

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response.status
      error.message = response.body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end
  end
end
