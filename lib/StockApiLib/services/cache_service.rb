require 'mongo'

module StockApiLib
  class CacheService
    def initialize(mongo_db:)
      @mongo = mongo_db
    end

    def update_cache(params:, data:, collection_name:)
      data = JSON.parse(data) if data.is_a? String
      params = query_params(params: params)
      document = {
        params: params,
        date: DateTime.now,
        data: data
      }
      res = @mongo[collection_name].find({params: params})
      res.delete_many
      @mongo[collection_name].insert_one(document)
    end

    def get_from_cache(params:, collection_name:, validity_time:)
      params = query_params(params: params)
      res = @mongo[collection_name].find({params: params}).sort(date: -1)
      if check_result_validity(res, validity_time)
        res.delete_many unless validity_time.nil?
        return nil
      end
      res.first['data']
    end

    def clear_collection(collection_name:, validity_time:)
      return if validity_time.nil?

      bottom_date = Time.now.getutc - validity_time
      @mongo[collection_name].find(
        date:
          { '$lt': bottom_date }
      ).delete_many
    end

    private

    def query_params(params: )
      params.compact.to_query
    end

    def check_result_validity(res, validity_time)
      validity_time.nil? || res.first.nil? ||
        Time.now.getutc - validity_time > res.first['date'].getutc
    end
  end
end
