module StockApiLib
  class LicenseHistoryService
    include StockApiLib::CommonApiHeaders

    def initialize(configuration:, http_service: HttpService.new)
      @configuration = configuration
      @http_service = http_service
    end

    # @param [LicenseHistoryRequest] request
    # @param [String] access_token
    # @return [LicenseHistoryResponse]
    def get_license_history_files(request:, access_token:)
      validate_search_params(request: request, access_token: access_token)
      endpoint = @configuration.endpoints[:license_history]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(
        configuration: @configuration,
        access_token: access_token
      )
      @http_service.get(url: url, headers: headers)
    end

    def prepare(response:)
      json = response.body
      return LicenseHistoryResponse.from(json: json) if response.status == 200

      ApiError.from(json: json) if response.status == 400

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response.status
      error.message = response.body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end

    private

    def validate_search_params(request:, access_token:)
      raise 'Request cannot be null' if request.nil?
      if request.search_parameters.nil?
        raise 'Search parameter must be present in the request object'
      end
      raise 'Access token cannot be null or empty' if access_token.nil?
    end
  end
end
