module StockApiLib
  module CommonApiHeaders
    def generate_common_api_headers(configuration:, access_token: nil)
      headers = {}
      unless access_token.nil?
        headers['Authorization'] = 'Bearer ' + access_token
      end
      headers['x-api-key'] = configuration.api_key
      headers['x-product'] = configuration.product
      headers['x-request-id'] = SecureRandom.uuid
      headers
    end
  end
end
