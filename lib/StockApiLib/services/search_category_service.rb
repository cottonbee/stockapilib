require 'securerandom'

module StockApiLib
  class SearchCategoryService
    include StockApiLib::CommonApiHeaders

    def initialize(configuration:, http_service: HttpService.new)
      @configuration = configuration
      @http_service = http_service
    end

    def get_category(request:, access_token:)
      endpoint = @configuration.endpoints[:category]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(configuration: @configuration,
                                            access_token: access_token)
      @http_service.get(url: url, headers: headers)
    end

    def get_category_tree(request:, access_token:)
      endpoint = @configuration.endpoints[:category_tree]
      url = endpoint + '?' + URI.encode_www_form(request.to_hash)
      headers = generate_common_api_headers(configuration: @configuration,
                                            access_token: access_token)
      @http_service.get(url: url, headers: headers)
    end

    def prepare(response_body:, response_status: 200)
      json = response_body
      return SearchCategoryResponse.from(json: json) if response_status == 200

      ApiError.from(json: json)

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response.status
      error.message = response.body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end

    def prepare_category_tree(response_body:, response_status: 200)
      if response_status == 200
        json_array = response_body
        json_array = JSON.parse(json_array) if json_array.is_a? String
        return json_array.map do |json|
          SearchCategoryResponse.from(json: json)
        end
      end

      ApiError.from(json: json_array)

    rescue JSON::ParserError
      error = ApiError.new
      error.error_code = response_status
      error.message = response_body
      error.case = 'JSON_PARSE_ERROR'
      return error
    end


  end
end
