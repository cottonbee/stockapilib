module StockApiLib
  class StockApiService
    # @param [Configuration] configuration
    # @param [Mongo::Client] mongo_db
    def initialize(
      configuration:, mongo_db: nil, http_service: HttpService.new
    )
      @configuration = configuration
      @http_service = http_service
      @cache_service = CacheService.new(mongo_db: mongo_db) unless mongo_db.nil?
    end

    # Authorizarion

    def get_authorization_data(expiration_timestamp:)
      # not cached
      authorization_service.get_authorization_data(
        expiration_timestamp: expiration_timestamp
      )
    end

    # License

    def get_content_info(request:, access_token:)
      license_service.get_content_info(
        request: request, access_token: access_token
      )
    end

    def get_content_license(request:, access_token:)
      license_service.get_content_license(
        request: request, access_token: access_token
      )
    end

    def get_member_profile(request:, access_token:)
      license_service.get_member_profile(
        request: request, access_token: access_token
      )
    end

    def abandon_license(request:, access_token:)
      license_service.abandon_license(
        request: request, access_token: access_token
      )
    end

    def download_asset_url(request:, access_token:)
      license_service.download_asset_url(
        request: request, access_token: access_token
      )
    end

    def download_asset(request:, access_token:)
      license_service.download_asset(
        request: request, access_token: access_token
      )
    end

    # License history

    def get_license_history_files(request:, access_token:, validity_time: nil)
      raw_response = license_history_service.get_license_history_files(
        request: request, access_token: access_token
      )
      license_history_service.prepare(response: raw_response)
    end

    # Search category

    def get_category(request:, access_token:, validity_time: nil)
      data_from_cache = get_category_from_cache(
        request: request, validity_time: validity_time
      )
      return data_from_cache unless data_from_cache.nil?

      get_category_from_api(request: request, access_token: access_token)
    end

    def get_category_tree(request:, access_token:, validity_time: nil)
      data_from_cache = get_categories_from_cache(
        request: request, validity_time: validity_time
      )
      return data_from_cache unless data_from_cache.nil?

      get_categories_from_api(request: request, access_token: access_token)
    end

    # Search files

    def get_file(id:, locale: nil, validity_time: nil)
      data_from_cache = get_file_from_cache(
        id: id, locale: locale, validity_time: validity_time
      )
      return data_from_cache unless data_from_cache.nil?

      request = prepare_get_file_request(id, locale)
      response = get_files_from_api(request: request, access_token: nil)
      return response.files[0] if response.is_a?(StockApiLib::SearchFilesResponse) && response.nb_results == 1
      return nil if response.is_a?(StockApiLib::SearchFilesResponse) && response.nb_results !=1
      return response
    end

    def get_files(request:, access_token:, validity_time: nil)
      data_from_cache = get_files_from_cache(
        request: request,
        validity_time: validity_time
      )
      return data_from_cache unless data_from_cache.nil?

      get_files_from_api(request: request, access_token: access_token)
    end

    private

    def prepare_get_file_request(id, locale)
      request =
        StockApiLib::SearchFilesRequest.new
      request.search_parameters =
        StockApiLib::SearchParameters.new
      request.result_columns =
        Array.new(StockApiLib::StockFile.attributes)
      request.search_parameters.filters =
        StockApiLib::SearchParametersFilters.new
      request.locale = locale
      request.search_parameters.media_id = id
      request.result_columns.delete(:is_licensed)
      request.result_columns.delete(:asset_type_id)
      request.result_columns << :nb_results
      request
    end

    def get_category_from_api(request:, access_token:)
      raw_response = search_category_service.get_category(
        request: request, access_token: access_token
      )
      if raw_response.status == 200 && !@cache_service.nil?
        @cache_service.update_cache(
          params: sanitize_params(params: request.to_hash),
          data: JSON.parse(raw_response.body),
          collection_name: SearchCategoryResponse.name.downcase
        )
      end
      search_category_service.prepare(
        response_body: raw_response.body, response_status: raw_response.status
      )
    end

    def get_categories_from_api(request:, access_token:)
      raw_response = search_category_service.get_category_tree(
        request: request, access_token: access_token
      )
      if raw_response.status == 200 && !@cache_service.nil?
        update_get_categories_cache(
          json_data: JSON.parse(raw_response.body), params: request.to_hash
        )
      end
      search_category_service.prepare_category_tree(
        response_body: raw_response.body, response_status: raw_response.status
      )
    end

    def update_get_categories_cache(json_data:, params:)
      @cache_service.update_cache(
        params: params,
        data: json_data,
        collection_name: SearchCategoryResponse.name.downcase + '_tree'
      )
      if json_data.is_a? Array
        json_data.each do |category|
          @cache_service.update_cache(
            params: { category_id: category['id'], locale: params['locale'] },
            data: category,
            collection_name: SearchCategoryResponse.name.downcase
          )
        end
      end
    end

    def get_files_from_api(request:, access_token:)
      raw_response = search_files_service.get_files(
        request: request, access_token: access_token
      )
      if raw_response.status == 200
        update_get_files_cache(
          json_data: JSON.parse(raw_response.body),
          params: sanitize_params(params: request.to_hash)
        )
      end
      search_files_service.prepare(
        response_body: raw_response.body, response_status: raw_response.status
      )
    end

    def get_category_from_cache(request:, validity_time:)
      unless @cache_service.nil? || validity_time.nil?
        res = @cache_service.get_from_cache(
          params: sanitize_params(params: request.to_hash),
          collection_name: SearchCategoryResponse.name.downcase,
          validity_time: validity_time
        )

        unless res.nil?
          return search_category_service.prepare(response_body: res.body)
        end
      end
    end

    def get_categories_from_cache(request:, validity_time:)
      unless @cache_service.nil? || validity_time.nil?
        res = @cache_service.get_from_cache(
          params: sanitize_params(params: request.to_hash),
          collection_name: SearchCategoryResponse.name.downcase + '_tree',
          validity_time: validity_time
        )

        unless res.nil?
          return search_category_service.prepare_category_tree(response_body: res)
        end
      end
    end

    def get_files_from_cache(request:, validity_time:)
      unless @cache_service.nil? || validity_time.nil?
        res = @cache_service.get_from_cache(
          params: sanitize_params(params: request.to_hash),
          collection_name: SearchFilesResponse.name.downcase,
          validity_time: validity_time
        )
        return search_files_service.prepare(response_body: res) unless res.nil?
      end
      nil
    end

    def sanitize_params(params:)
      params.delete('result_columns[]')
      params
    end

    def update_get_files_cache(json_data:, params:)
      return if @cache_service.nil?

      @cache_service.update_cache(
        params: params, data: json_data,
        collection_name: SearchFilesResponse.name.downcase
      )
      if json_data['files'].is_a? Array
        json_data['files'].each do |file|
          update_stock_file_cache(locale: params['locale'], data: file)
        end
      end
    end

    def update_stock_file_cache(locale:, data:)
      return nil if @cache_service.nil?

      params = {
        locale: locale,
        id: data['id']
      }
      @cache_service.update_cache(
        params: params,
        data: data,
        collection_name: StockFile.name.downcase
      )
    end

    def get_file_from_cache(id:, locale:, validity_time:)
      unless @cache_service.nil? || validity_time.nil?

        params = { id: id }
        params[:locale] = locale unless locale.nil?

        res = @cache_service.get_from_cache(
          params: sanitize_params(params: params),
          collection_name: StockFile.name.downcase,
          validity_time: validity_time
        )
        return StockFile.from(json: res) unless res.nil?
      end
      nil
    end

    def flatten_params(hash:)
      hash.each do |key, value|

      end
    end

    # @return [AuthorizationService]
    def authorization_service
      if @_authorization_service.nil?
        @_authorization_service = AuthorizationService.new(
          configuration: @configuration,
          http_service: @http_service
        )
      end
      @_authorization_service
    end

    def license_history_service
      if @_license_history_service.nil?
        @_license_history_service = LicenseHistoryService.new(
          configuration: @configuration,
          http_service: @http_service
        )
      end
      @_license_history_service
    end

    def license_service
      if @_license_service.nil?
        @_license_service = LicenseService.new(
          configuration: @configuration,
          http_service: @http_service
        )
      end
      @_license_service
    end

    def search_category_service
      if @_search_category_service.nil?
        @_search_category_service = SearchCategoryService.new(
          configuration: @configuration,
          http_service: @http_service
        )
      end
      @_search_category_service
    end

    def search_files_service
      if @_search_files_service.nil?
        @_search_files_service = SearchFilesService.new(
          configuration: @configuration,
          http_service: @http_service
        )
      end
      @_search_files_service
    end
  end
end
