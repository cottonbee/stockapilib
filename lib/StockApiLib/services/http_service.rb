module StockApiLib
  class HttpService
    def initialize(http_client_class: Faraday, log: false)
      @http_client_class = http_client_class

      @connection = Faraday.new do |conn|
        conn.request :multipart
        conn.request :url_encoded
        if log
          logger = Logger.new('log/stock_api_http.txt')
          logger.level = Logger::DEBUG

          conn.response :logger, logger, bodies: true do |logger|
            logger.filter("\n", "\n[HTTP-STOCK] ")
          end
        end
        conn.use FaradayMiddleware::FollowRedirects
        conn.adapter :net_http
      end
    end

    def get(url:, headers:)
      @connection.get url do |request|
        request.headers = headers
      end
    end

    def post(url:, headers:, body:)
      @connection.post url do |request|
        request.headers = headers
        request.body = body
      end
    end

    def post_multipart(url:, headers:,
                       filename_or_io:,
                       content_type: 'image/jpeg', file_name: nil)
      body = { image: Faraday::UploadIO.new(filename_or_io,
                                            content_type, file_name) }
      @connection.post url do |request|
        request.headers = headers
        request.body = body
      end
    end
  end
end
